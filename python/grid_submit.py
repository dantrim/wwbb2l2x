#!/bin/env python

from __future__ import print_function # jic

#
# script to submit WWbb2l2x jobs to the grid
#
# author: daniel.joseph.antrim@CERN.CH
#         dantrim@UCI.EDU
# date: February 2019
#

import os, sys, re
import argparse
import subprocess

def is_interesting_line(line = "", regexp = "") :
    """
    Check if this is an interesting line. An interesting line
    is not a comment, is non-empty, and is matching an expression
    """
    line = line.strip()
    tokens = line.split()
    return (len(line) > 0 and not line.startswith("#") and len(tokens) == 1 and re.search(regexp, line))

def determine_sample_name(input_dataset_name = "") :

    sample = re.sub(".merge.*", "", input_dataset_name)
    return sample

def isMC16a(input_dataset_name) :
    if "_r9364_" in input_dataset_name :
        return True
    else :
        return False

def isMC16d(input_dataset_name) :
    if "_r10201_" in input_dataset_name :
        return True
    else :
        return False

def isMC16e(input_dataset_name) :
    if '_r10724_' in input_dataset_name :
        return True
    else :
        return False

def determine_outdataset_name(input_dataset_name, tag, use_group, nickname, prun_suffix = "wwbb2l2x.root") :

    prefix = "group.phys-susy." if use_group else "user.{}.".format(nickname)
    output_ds_name = prefix + re.sub("/", "", input_dataset_name) + "_" + tag + "/"
    n_daod_types = 30
    daod_range = range(n_daod_types)
    for i in daod_range[::-1] :
        output_ds_name = re.sub("DAOD_SUSY%s" % str(i), "WWbb2l2x", output_ds_name)
    output_ds_name = re.sub("AOD", "WWbb2l2x", output_ds_name)
    output_ds_name = re.sub("merge\.", "", output_ds_name)
    output_ds_name = re.sub("deriv\.", "", output_ds_name)
    output_ds_name = re.sub("MAXHTPTV", "", output_ds_name)
    output_ds_name = re.sub("Filter", "Filt", output_ds_name)
    output_ds_name = re.sub("CVetoBVeto", "CVBV", output_ds_name)
    output_ds_name = re.sub('BFilter', 'BF', output_ds_name)
    output_ds_name = re.sub('CFilter', 'CF', output_ds_name)
    output_ds_name = re.sub('BVeto', 'BV', output_ds_name)
    output_ds_name = re.sub('CVeto', 'CV', output_ds_name)
    output_ds_name = re.sub('UEEE3_CTEQ6L1_', '', output_ds_name)
    output_ds_name = re.sub('AUET2CTEQ6L1_', '', output_ds_name)
    output_ds_name = re.sub('AUET3CTEQ6L1_', '', output_ds_name)
    output_ds_name = re.sub('AUET2BCTEQ6L1_', '', output_ds_name)
    output_ds_name = re.sub('AU2CT10_', '', output_ds_name)
    output_ds_name = re.sub('AUET2B_CTEQ6L1_', '', output_ds_name)
    output_ds_name = re.sub('_NNPDF30NNLO', '', output_ds_name)
    output_ds_name = re.sub('_NN30NNLO', '', output_ds_name)
    output_ds_name = re.sub('PowhegPythia', 'PP', output_ds_name)
    output_ds_name = re.sub('MadGraphPythia', 'MGP', output_ds_name)
    output_ds_name = re.sub('aMcAtNloPythia', 'AMCP', output_ds_name)
    output_ds_name = re.sub('Sherpa_221', 'Sherpa221', output_ds_name)
    output_ds_name = re.sub('_A14NNPDF23LO', '', output_ds_name)
    output_ds_name = re.sub('EvtGen', '', output_ds_name)
    output_ds_name = re.sub('Pythia8', 'P8', output_ds_name)
    output_ds_name = re.sub('_A14_NNPDF23_NNPDF30ME', '', output_ds_name)
    if output_ds_name.count("group.phys-susy.") > 1 :
        output_ds_name = output_ds_name.replace("group.phys-susy.", "", 1)
    max_ds_length = 132 # still valid?
    if True :
        tags_to_keep = "_.*_%s" % tag # drop n-2 tags
        regex = "\.WWbb2l2x\.(?P<other_tags>.*)%s" % tags_to_keep
        match = re.search(regex, output_ds_name)
        if match :
            output_ds_name = output_ds_name.replace(match.group("other_tags"), "")
    output_ds_name = output_ds_name.replace("__","_").replace("..",".").replace("_.", ".").replace("._",".")
    return output_ds_name

def main() :

    parser = argparse.ArgumentParser(description = "submit wwbb2l2x jobs to the grid")
    add_arg = parser.add_argument
    add_arg("-f", "--input-files", required = True, nargs = "+",
        help = "Input text file with datasets, can specify more than one"
    )
    add_arg("-n", "--nickname", required = True,
        help = "Grid nickname, for naming output DS"
    )
    add_arg("-t", "--tag", required = True,
        help = "Tag to add to output dataset name"
    )
    add_arg("--nFilesPerJob", default = "5",
        help = "Set number of files to run per grid sub-job in a given task"
    )
    add_arg("--group-role", action = "store_true",
        help = "Submit jobs with group production role"
    )
    add_arg("-p", "--pattern", default = ".*",
        help = "grep pattern to select datasets from input list"
    )
    add_arg("--slim", action = "store_true",
        help = "Run WWbb2l2x with slimming"
    )
    args = parser.parse_args()

    input_files = args.input_files
    cmtconfig = os.getenv("CMTCONFIG","")
    if not cmtconfig :
        print("ERROR CMTCONFIG environment not set, have you set up a release?")
        sys.exit()

    print("Submitting")
    print("input file   : {}".format(input_files))
    print("pattern      : {}".format(args.pattern))
    print("tag          : {}".format(args.tag))
    for input_file in input_files :
        with open(input_file) as lines :
            lines = [l.strip() for l in lines if is_interesting_line(line = l, regexp = args.pattern)]
            for line in lines :
                inDS = line
                sample = determine_sample_name(inDS)
                out_ds_suffix = "nt"
                outDS = determine_outdataset_name(input_dataset_name = inDS, tag = args.tag,
                            use_group = args.group_role, nickname = args.nickname, prun_suffix = "_" + out_ds_suffix)
                is_mc16a_sample = isMC16a(inDS)
                is_mc16d_sample = isMC16d(inDS)
                is_mc16e_sample = isMC16e(inDS)

                if is_mc16a_sample :
                    outDS = outDS.replace("WWbb2l2x", "WWbb2l2x.mc16a")
                if is_mc16d_sample :
                    outDS = outDS.replace("WWbb2l2x", "WWbb2l2x.mc16d")
                if is_mc16e_sample :
                    outDS = outDS.replace("WWbb2l2x", "WWbb2l2x.mc16e")

                # construct the command to run on the grid site
                grid_command = "./bash/grid_script.sh %IN "
                #grid_command = "../source/wwbb2l2x/bash/grid_script.sh %IN "
                grid_command += (" --uncerts ")
                #print(" ****** WARNING FIXING NUMBER OF EVENTS TO PROCESS ******* ")
                #print(" ****** WARNING FIXING NUMBER OF EVENTS TO PROCESS ******* ")
                #print(" ****** WARNING FIXING NUMBER OF EVENTS TO PROCESS ******* ")
                #print(" ****** WARNING FIXING NUMBER OF EVENTS TO PROCESS ******* ")
                #print(" ****** WARNING FIXING NUMBER OF EVENTS TO PROCESS ******* ")
                #grid_command += (" -n 50 ")
                grid_command += (" --slim " if args.slim else "")

                line_break = "=" * 90
                print("\n{}\nInput {}\nOutput {}\nSample {}\nCommand {}".format(line_break, inDS, outDS, sample, grid_command))

                # construct the prun command
                prun_command = ('prun --exec "' + grid_command + '" --useAthenaPackages --tmpDir /tmp ') # R21
                prun_command += (' --inDS {} --outDS {}'.format(inDS, outDS))
                prun_command += (' --inTarBall=area.tgz --extFile "*.so,*.root" --match "*root*"')
                prun_command += (" --safetySize=600")
                prun_command += (' --outputs "{0}:wwbb2l2x.root"'.format(out_ds_suffix))
                prun_command += (" --nFilesPerJob={}".format(args.nFilesPerJob))
                prun_command += (" --cmtConfig={}".format(cmtconfig))
                prun_command += ('' if not args.group_role else ' --official --voms atlas:/atlas/phys-susy/Role=production')
                prun_command += (' --destSE=' + (args.destSE if not args.group_role else
                                                ','.join(['MWT2_UC_SCRATCHDISK', 'SLACXRD_SCRATCHDISK'])))

                print("Calling: {}".format(prun_command))
                subprocess.call(prun_command, shell = True)

if __name__ == "__main__" :
    main()

#!/bin/env python

from __future__ import print_function
import sys, os, glob
import argparse

def main() :

    parser = argparse.ArgumentParser(description = "Get the total sum of event weights across a set of WWbb2l2x inputs")
    parser.add_argument("-i", "--input", nargs = "+", required = True,
        help = "Provide (a set of) inputs"
    )
    parser.add_argument("-v", "--verbose", action = "store_true", default = False,
        help = "Be loud about it"
    )
    args = parser.parse_args()

    import ROOT as r
    r.gROOT.SetBatch(True)
    r.PyConfig.IgnoreCommandLineOptions = True

    if len(args.input) == 1 :
        dsid = args.input[0].strip().split("mc16_13TeV.")
        dsid = dsid[1].split(".")[0]

    if args.verbose :
        print("Calculating sumw for sample composed of {} datasets".format(len(args.input)))

    total_sumw = 0.0
    for input_dataset in args.input :
        # we exect that an "input" is a directory containing all the files for a given dataset
        full_path = os.path.abspath(input_dataset)
        dataset_files = glob.glob("{}/*.root".format(full_path))
        n_files = len(dataset_files)
        if args.verbose :
            print("Found {} files in dataset {}".format(n_files, input_dataset.strip().split("/")[-1]))
        for ifile, dataset_file in enumerate(dataset_files) :
            rfile = r.TFile.Open(dataset_file)
            tree = rfile.Get("wwbb") # assume fixed treename
            for ievent, event in enumerate(tree) :
                if ievent > 0 : break
                sumw = event.nom_sumw
                if args.verbose :
                    print(" [%03d/%03d] %.2f" %(ifile, n_files-1, sumw)) 
                total_sumw += sumw

    if len(args.input) == 1 :
        print("{} {}".format(dsid, total_sumw))
    else :
        print("Total sumw: {}".format(total_sumw))

if __name__ == "__main__" :
    main()

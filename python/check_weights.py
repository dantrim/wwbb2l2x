#!/bin/env python

from __future__ import print_function
import sys, os, argparse
import glob

def main() :

    parser = argparse.ArgumentParser(description = "Check weight names in input files")
    parser.add_argument("-i", "--input", required = True, nargs = "+",
        help = "Input datasets directories"
    )
    parser.add_argument("--strict", default = False, action = "store_true",
        help = "Require the weight names to be the same across all input datasets"
    )
    args = parser.parse_args()

    import ROOT as r
    r.gROOT.SetBatch(True)
    r.PyConfig.IgnoreCommandLineOptions = True

    weight_names = {}

    print("Checking %d input datasets" % len(args.input))
    w_set = set()

    n_opened = 0
    for ii, input_name in enumerate(args.input) :
        input_name = os.path.abspath(input_name)
        infiles = glob.glob("%s/*.root" % input_name)
        found_nonempty = False
        for ifile in infiles :
            if found_nonempty : break
            rfile = r.TFile.Open(ifile)
            tree = rfile.Get("wwbb")
            if tree.GetEntriesFast() > 10 :
                found_nonempty = True
            n_opened += 1
            for ievent, event in enumerate(tree) :
                if ievent > 0 : break
                wnames = event.weight_names
                if input_name not in weight_names :
                    weight_names[input_name] = []
                for w in wnames :
                    w = w.strip()
                    if w not in weight_names[input_name] :
                        weight_names[input_name].append(w)
                        if ii == 0 or len(w_set) == 0 :
                            w_set.add(w)
                        else :
                            if w not in w_set :
                                print("FOUND UNACCOUNTED FOR WEIGHT NAME: %s %s" % (w, input_name))
                    

    print("Opened %d files" % n_opened)

    weight_lengths = {}
    max_w = -1
    max_weight_ds = ""
    length_set = set()
    for input_name, weight_vec in weight_names.items() :
        n_w = len(weight_vec)
        weight_lengths[input_name] = n_w
        length_set.add(n_w)
        if n_w > max_w :
            max_w = n_w
            max_weight_ds = input_name
    if len(length_set) != 1 :
        print("WARNING Found datasets with different weight names")
        print(" > length_set = {}".format(length_set))
    print("Max number of weights: %d" % max_w)
    print("max weight ds = {}".format(max_weight_ds))
    for iw, w in enumerate(weight_names[max_weight_ds]) :
        #print(" [%03d/%03d] %s" % (iw, max_w-1, w))
        print(w)
    

#    n_w = len(weight_names)
#    print("Found %d unique weight names" % n_w)
#    for i, w in enumerate(weight_names) :
#        print(" > [%03d/%03d] : %s" % (i, n_w-1, w))
        

if __name__ == "__main__" :
    main()

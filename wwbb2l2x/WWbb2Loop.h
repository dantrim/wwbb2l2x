#ifndef WWBB2LOOP_H
#define WWBB2LOOP_H

//ROOT
#include "TSelector.h"
#include "TStopwatch.h"
#include "TTree.h"
#include "TChain.h"
#include "TDirectory.h"
#include "TFile.h"

//xAOD
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "xAODEventInfo/EventInfo.h"
namespace xAOD {
    class TEvent;
    class TStore;
}
#include "SUSYTools/SUSYObjDef_xAOD.h"

//CONTAINERS
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"

//ASG
#include "AsgTools/ToolHandle.h"
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "MuonAnalysisInterfaces/IMuonEfficiencyScaleFactors.h"
#include "EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h"
#include "EgammaAnalysisInterfaces/IAsgElectronEfficiencyCorrectionTool.h"
#include "FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h"
#include "FTagAnalysisInterfaces/IBTaggingSelectionTool.h"
#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#include "PMGTools/PMGTruthWeightTool.h"


//lwtnn
#include "lwtnn/LightweightGraph.hh"
#include "lwtnn/parse_json.hh"

//std/stl
#include <map>

//ROOT
#include "TH1F.h"

namespace wwbb {

struct pt_greater {
    bool operator() (const xAOD::IParticle* a, const xAOD::IParticle* b) { return a->pt() > b->pt(); }
};
static pt_greater PtGreater;

struct pt_greaterJet {
    bool operator() (const xAOD::Jet* a, const xAOD::Jet* b) { return a->pt() > b->pt(); }
};
static pt_greaterJet PtGreaterJet;

class WWbb2Loop : public TSelector
{
    public :
        WWbb2Loop();
        virtual ~WWbb2Loop(){};

        void set_verbose(bool doit) { m_verbose = doit; }
        bool verbose() { return m_verbose; }

        void set_chain(TChain* chain) { m_chain = chain; }
        TChain* input_chain() { return m_chain; }

        void set_flavor(std::string flav) { _do_flav = flav; }
        std::string do_flav() { return _do_flav; }

        void set_muon_in_jet(bool doit) { _do_muon_jet = doit; }
        bool muon_jet() { return _do_muon_jet; }


        void set_uncerts(bool doit) { m_do_uncerts = doit; }
        bool do_uncerts() { return m_do_uncerts; }

        void set_suffix(std::string suf) { m_suffix = suf; }
        std::string suffix() { return m_suffix; }

        void set_do_slim(bool doit) { m_do_slim = doit; }
        bool do_slim() { return m_do_slim; }

        void set_print_weights(bool doit) { m_print_weights = doit; }
        bool do_print_weights() { return m_print_weights; }
        void print_weights();

        bool get_metadata_info(TTree* tree, bool & is_derivation, std::string & simflavor, std::string & amitag);
        TDirectory* get_directory_from_chain(TTree* tree);

        void get_sumw();
        bool collect_cutbooks(xAOD::TEvent& event, int file_counter);


        bool mc() { return m_is_mc; }

        TStopwatch* timer() { return &m_timer; }
        std::string timer_summary();

        ST::SUSYObjDef_xAOD* susyObj() { return _susyObj; }

        xAOD::TEvent* event() { return m_event; }
        xAOD::TStore* store() { return m_store; }

        void initialize_cutflow();
        void pass_cut(std::string cutname = "");

        bool initialize_susytools();
        bool initialize_grl();
        bool initialize_muon_jet_outputs();
        bool initialize_truth_weight_tool();
        bool initialize_lwtnn();
        bool initialize_outputs();
        bool load_objects();
        bool clear_objects();
        bool prepare_for_event(Long64_t entry);
        bool pass_event_cleaning();
        void test_cutflow();
        void print_cutflow();
        bool pass_flavor(const std::vector<xAOD::IParticle*> leptons, std::string flavor_string);
        bool lepton_is_electron(const xAOD::IParticle* particle);
        bool lepton_is_muon(const xAOD::IParticle* particle);
        bool pass_zveto(const std::vector<xAOD::IParticle*> leptons, float window_width = 20. /*GeV*/);

        void do_muon_jet();
        void write_muon_jet_outputs();

        void process_uncerts();

        std::vector< std::string > ilumicalc_files();
        std::vector< std::string > prw_files();

        //////////////////////////////////////////////////////
        // TSelector overrides
        //////////////////////////////////////////////////////
        virtual void Init(TTree* tree); // Called every time a new TTree is attached
        //virtual void SlaveBegin(TTree* tree);
        virtual void Begin(TTree* tree); // Called before looping on entries
        virtual Bool_t Notify() { return kTRUE; } // Called at the first entry of a new file
        virtual void Terminate(); // called after looping is finished
        virtual Int_t Version() const { return 2; } // adhere to stupid ROOT design
        virtual Bool_t Process(Long64_t entry); // main event loop function

        //////////////////////////////////////////////////////
        // Object Getters
        //////////////////////////////////////////////////////
        xAOD::ElectronContainer* xaod_electrons() { return _electrons; }
        xAOD::MuonContainer* xaod_muons() { return _muons; }
        xAOD::JetContainer* xaod_jets() { return _jets; }
        xAOD::MissingETContainer* xaod_met() { return _met; }
        xAOD::PhotonContainer* xaod_photons() { return _photons; }
        xAOD::TauJetContainer* xaod_taus() { return _taus; }

        std::vector<xAOD::IParticle*> base_electrons() { return _base_electrons; }
        std::vector<xAOD::IParticle*> sig_electrons() { return _sig_electrons; }
        std::vector<xAOD::IParticle*> pre_muons() { return _pre_muons; }
        std::vector<xAOD::IParticle*> base_muons() { return _base_muons; }
        std::vector<xAOD::IParticle*> sig_muons() { return _sig_muons; }
        std::vector<xAOD::IParticle*> base_leptons() { return _base_leptons; }
        std::vector<xAOD::IParticle*> sig_leptons() { return _sig_leptons; }
        std::vector<xAOD::Jet*> base_jets() { return _base_jets; }
        std::vector<xAOD::Jet*> sig_jets() { return _sig_jets; }
        std::vector<xAOD::Photon*> base_photons() { return _base_photons; }
        std::vector<xAOD::Photon*> sig_potons() { return _sig_photons; }
        std::vector<xAOD::TauJet*> base_taus() { return _base_taus; }
        std::vector<xAOD::TauJet*> sig_taus() { return _sig_taus; }

        // OUTPUTS
        TFile* output_file() { return _rfile; }
        TTree* output_tree() { return _rtree; }


    private :

        bool m_is_derivation;
        bool m_is_af2;
        std::string m_mctype;
        bool m_verbose;
        TChain* m_chain;
        std::string _do_flav;
        bool _do_muon_jet;
        bool m_is_mc;
        bool m_do_uncerts;
        bool m_print_weights;
        std::string m_suffix;
        bool m_do_slim;

        double m_nEventsProcessed;
        double m_sumOfWeights;
        double m_sumOfWeightsSquared;

        TStopwatch m_timer;
        int n_events_processed;

        //xAOD
        xAOD::TEvent* m_event;
        xAOD::TStore* m_store;

        //TOOLS
        ST::SUSYObjDef_xAOD* _susyObj;
        asg::AnaToolHandle<IAsgElectronLikelihoodTool> _ele_idtool_baseline;
        asg::AnaToolHandle<IAsgElectronLikelihoodTool> _ele_idtool_signal;
        asg::AnaToolHandle<CP::IMuonSelectionTool> _mu_idtool_baseline;
        asg::AnaToolHandle<CP::IMuonSelectionTool> _mu_idtool_signal;
        asg::AnaToolHandle<IBTaggingSelectionTool> _btag_sel_tool;
        asg::AnaToolHandle<IBTaggingEfficiencyTool> _btag_eff_tool;
        asg::AnaToolHandle<IGoodRunsListSelectionTool> _grl_tool;
        asg::AnaToolHandle<PMGTools::IPMGTruthWeightTool> _weightTool;

        //object containers
        xAOD::ElectronContainer* _electrons;
        xAOD::ShallowAuxContainer* _electrons_aux;
        xAOD::MuonContainer* _muons;
        xAOD::ShallowAuxContainer* _muons_aux;
        xAOD::JetContainer* _jets;
        xAOD::ShallowAuxContainer* _jets_aux;
        xAOD::MissingETContainer* _met;
        xAOD::MissingETAuxContainer* _met_aux;
        xAOD::PhotonContainer* _photons;
        xAOD::ShallowAuxContainer* _photons_aux;
        xAOD::TauJetContainer* _taus;
        xAOD::ShallowAuxContainer* _taus_aux;


        std::vector<xAOD::IParticle*> _base_electrons;
        std::vector<xAOD::IParticle*> _sig_electrons;
        std::vector<xAOD::IParticle*> _pre_muons;
        std::vector<xAOD::IParticle*> _base_muons;
        std::vector<xAOD::IParticle*> _sig_muons;
        std::vector<xAOD::IParticle*> _base_leptons;
        std::vector<xAOD::IParticle*> _sig_leptons;
        std::vector<xAOD::Jet*> _base_jets;
        std::vector<xAOD::Jet*> _sig_jets;
        std::vector<xAOD::Photon*> _base_photons;
        std::vector<xAOD::Photon*> _sig_photons;
        std::vector<xAOD::TauJet*> _base_taus;
        std::vector<xAOD::TauJet*> _sig_taus;

        // counters
        std::vector< std::string> _cut_names;
        std::map< std::string, long long int > _raw_cut_map;

        // muon jet
        TH1F* h_n_dR1;
        TH1F* h_n_dR04;
        TH1F* h_n_dR02;
        TH1F* h_mbb_uncorr;
        TH1F* h_mbb_corr;

        // NN
        lwt::LightweightGraph* _lwt;
        std::map<std::string, double> _lwt_map;

        // OUTPUTS
        TFile* _rfile;
        TTree* _rtree;

        std::vector<std::string>* b_weight_names;
        std::vector<float>* b_weights;
        //std::vector<float>* b_weight_deltas; // (variation - nominal) / nominal
        double b_nom_weight;
        int b_dsid;
        int b_mctype;
        double b_nom_sumw;
        double b_n_in_aod;

        // leptons
        int b_isEE;
        int b_isMM;
        int b_isEM;
        int b_isME;
        int b_isSF;
        int b_isDF;
        float b_l0_pt;
        float b_l1_pt;
        float b_l0_eta;
        float b_l1_eta;
        float b_l0_phi;
        float b_l1_phi;
        float b_mll;
        float b_pTll;
        float b_dphi_ll;
        float b_dRll;

        // met
        float b_met;
        float b_metPhi;

        // leptons+MET
        float b_dphi_met_ll;
        float b_met_pTll;

        // jets
        int b_nJets;
        int b_nBJets;
        int b_nSJets;
        float b_j0_pt;
        float b_j1_pt;
        float b_j2_pt;
        float b_bj0_pt;
        float b_bj1_pt;
        float b_bj2_pt;
        float b_sj0_pt;
        float b_sj1_pt;
        float b_sj2_pt;
        float b_j0_eta;
        float b_j1_eta;
        float b_j2_eta;
        float b_bj0_eta;
        float b_bj1_eta;
        float b_bj2_eta;
        float b_sj0_eta;
        float b_sj1_eta;
        float b_sj2_eta;
        float b_j0_phi;
        float b_j1_phi;
        float b_j2_phi;
        float b_bj0_phi;
        float b_bj1_phi;
        float b_bj2_phi;
        float b_sj0_phi;
        float b_sj1_phi;
        float b_sj2_phi;

        // bb
        float b_mbb;
        float b_dphi_bb;
        float b_dRbb;

        // jets and leptons
        float b_dR_ll_bb;
        float b_dphi_ll_bb;
        float b_dphi_j0_ll;
        float b_dphi_j0_l0;
        float b_dphi_sj0_ll;
        float b_dphi_sj0_l0;
        float b_dphi_bj0_ll;
        float b_dphi_bj0_l0;

        float b_mt2_ll;
        float b_mt2_bb;
        float b_mt2_llbb;

        float b_HT2;
        float b_HT2Ratio;

        float b_MT_1;
        float b_MT_1_scaled;

        // NN
        float b_NN_p_hh;
        float b_NN_p_top;
        float b_NN_p_zsf;
        float b_NN_p_ztt;
        float b_NN_d_hh;
        float b_NN_d_top;
        float b_NN_d_zsf;
        float b_NN_d_ztt;

        
        
        
        

}; // class WWbb2Loop


} // namespace wwbb


#endif

//util
#include "wwbb2l2x/WWbb2Loop.h"
#include "wwbb2l2x/utility/ChainHelper.h"

//std/stl
#include <iostream>
#include <fstream>
using namespace std;

//boost
#include <boost/program_options.hpp>

//xAOD
#include "xAODRootAccess/Init.h"

int main(int argc, char** argv)
{

    string algo_name = string(argv[0]);
    string _input_file = "";
    long long _n_to_process = -1;
    bool _debug = false;
    bool do_muon_jet_corr = false;
    string dilepton_flavor = "ee";
    bool do_uncerts = false;
    bool print_weights = false;
    string suffix = "";
    bool do_slim = false;

    namespace po = boost::program_options;
    string description = "Run WWbb2L2X Analysis";
    po::options_description desc(description);
    desc.add_options()
        ("help,h", "print out the help message")
        ("input,i",
            po::value(&_input_file),
                "input file [DAOD, text filelist, or directory of DAOD files]")
        ("nentries,n",
            po::value(&_n_to_process)->default_value(-1),
                "number of entries to process [default: all]")
        ("verbose,v",
            po::value(&_debug),
                "turn on verbose mode")
        ("flavor",
            po::value(&dilepton_flavor),
                "set dilepton flavor to do [default: ee]")
        ("muonjet",
                "look at muon-in-jet correction [default: false]")
        ("uncerts",
                "do WWbb modelling uncerts [default: false]")
        ("print-weights",
                "if doing uncertainties, print the weight names and exit [default: false]")
        ("suffix,s",
            po::value(&suffix),
                "add a suffix to any of the outputs [default: none]")
        ("slim",
            "run with strict event slimming (OR of WWbb2L analysis regions) [default: false]")
    ;
    po::variables_map vm;
    po::store(po::command_line_parser(argc,argv).options(desc).run(), vm);
    po::notify(vm);

    if(vm.count("help")) {
        cout << desc << endl;
        return 0;
    }
    if(vm.count("muonjet"))
    {
        do_muon_jet_corr = true;
    }
    if(vm.count("uncerts"))
    {
        do_uncerts = true;
    }
    if(vm.count("print-weights"))
    {
        print_weights = true;
    }
    if(vm.count("slim"))
    {
        do_slim = true;
    }

    if(!vm.count("input")) {
        cout << "ERROR You did not provide an input file/list/dir" << endl;
        return 1;
    }

    if(!(dilepton_flavor == "ee" || dilepton_flavor == "mm" || dilepton_flavor == "df") && !do_muon_jet_corr)
    {
        cout << "Invalid dilepton flavor" << endl;
        return 1;
    }

    //std::ifstream user_input_fs(_input_file);
    //if(!user_input_fs.good()) {
    //    cout << "ERROR Provided input (" << _input_file << ") not found" << endl;
    //    return 1;
    //}

    xAOD::Init("WWbb2L2X");
    TChain* chain = new TChain("CollectionTree");
    int file_err = ChainHelper::addInput(chain, _input_file, true);
    if(file_err) return 1;
    chain->ls();
    Long64_t n_in_chain = chain->GetEntries();
    if(_n_to_process < 0) _n_to_process = n_in_chain;
    if(_n_to_process > n_in_chain) _n_to_process = n_in_chain;

    // build the looper
    wwbb::WWbb2Loop* analysis = new wwbb::WWbb2Loop();
    analysis->set_verbose(_debug);
    analysis->set_chain(chain);
    analysis->set_flavor(dilepton_flavor);
    analysis->set_muon_in_jet(do_muon_jet_corr);
    analysis->set_uncerts(do_uncerts);
    analysis->set_print_weights(print_weights);
    analysis->set_suffix(suffix);
    analysis->set_do_slim(do_slim);

    cout << "=================================================================" << endl;
    cout << algo_name << "    Total number of entries in the input chain : " << n_in_chain << endl;
    cout << algo_name << "    Total number of entries to process         : " << _n_to_process << endl;
    cout << "=================================================================" << endl;
    chain->Process(analysis, _input_file.c_str(), _n_to_process);

    cout << algo_name << "    Done." << endl;
    delete analysis;
    delete chain;

    return 0;
}

